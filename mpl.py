# importing mplot3d toolkits, numpy and matplotlib
from mpl_toolkits import mplot3d
import numpy as np
import matplotlib.pyplot as plt
 
x = np.outer(np.linspace(-3, 3, 32), np.ones(32))
y = x.copy().T # transpose
z = 0.26*(x**2 + y**2) - 0.48 * x*y
 
# Creating figure
fig = plt.figure(figsize =(10, 7))
ax = plt.axes(projection ='3d')
 
# Creating color map
my_cmap = plt.get_cmap('hot')

surf = ax.plot_surface(x, y, z,
                       cmap = my_cmap,
                       edgecolor ='none')
 
fig.colorbar(surf, ax = ax,
             shrink = 0.5, aspect = 5)
 
ax.set_title('Surface plot')
 
# show plot
plt.show()

""" # Creating plot
ax.plot_surface(x, y, z)
 
# show plot
plt.show() """