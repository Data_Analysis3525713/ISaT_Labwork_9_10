import numpy as np
from geneticalgorithm import geneticalgorithm as ga

def f(X):
    return 0.26 * (X[0]**2 + X[1]**2) - 0.48 * X[0]*X[1]

def basicGA(X):
    if X[2] > X[4]: return 100000.0
    bga_algorithm_params = {
        'max_num_iteration': 30,\
        'population_size':X[0],\
        'mutation_probability':X[1],\
        'elit_ratio': X[2],\
        'crossover_probability': X[3],\
        'parents_portion': X[4],\
        'crossover_type':'uniform',\
        'max_iteration_without_improv':None
    }
    bga_varbounds = np.array([[-10,10],[-10,10]])
    bga_model = ga(function=f, 
                   dimension=2, 
                   variable_type='real', 
                   variable_boundaries=bga_varbounds, 
                   algorithm_parameters=bga_algorithm_params,
                   convergence_curve=False,
                   progress_bar=True)
    bga_results = []
    for i in range(3):
        bga_model.run()
        bga_results.append(bga_model.output_dict['function'])

    """ print(bga_model.param)
    print('---------------------------------------------------------') """

    #return bga_model.output_dict['function']
    return np.var(bga_results)

#basicGA(np.array([100,100,0.1,0.01,0.5,0.3]))
h_variable_types = np.array([['int'],['real'],['real'],['real'],['real']])
h_varbounds = np.array([[1,10],[0,1],[0,1],[0,1],[0,1]])

h_algorithm_params = {
    'max_num_iteration': 10,\
    'population_size':10,\
    'mutation_probability':0.1,\
    'elit_ratio': 0.01,\
    'crossover_probability': 0.5,\
    'parents_portion': 0.3,\
    'crossover_type':'uniform',\
    'max_iteration_without_improv':None
}
h_model = ga(function=basicGA, 
           dimension=5, 
           variable_type_mixed=h_variable_types, 
           variable_boundaries=h_varbounds,
           function_timeout=240,
           algorithm_parameters=h_algorithm_params)
#print(f"-----------------\nmodel.param=\n{h_model.param}\n-------------")
h_model.run()



















""" def f(X):
    return 0.26 * (X[0]**2 + X[1]**2) - 0.48 * X[0]*X[1]

varbounds = np.array([[-10,10],[-10,10]])
algorithm_param = {
    'max_num_iteration': None,\
    'population_size':100,\
    'mutation_probability':0.1,\
    'elit_ratio': 0.01,\
    'crossover_probability': 0.5,\
    'parents_portion': 0.3,\
    'crossover_type':'uniform',\
    'max_iteration_without_improv':None
}

algorithm_param = {
    'max_num_iteration': 100,\
    'population_size':100,\
    'mutation_probability':0.1,\
    'elit_ratio': 0.01,\
    'crossover_probability': 0.5,\
    'parents_portion': 0.3,\
    'crossover_type':'uniform',\
    'max_iteration_without_improv':None
}

model = ga(function=f, dimension=2, variable_type='real', variable_boundaries=varbounds, algorithm_parameters=algorithm_param)
#print(model.param)

model.run() """