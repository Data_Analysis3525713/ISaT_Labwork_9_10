import numpy as np
from geneticalgorithm import geneticalgorithm as ga
import time
from scipy import optimize
import matplotlib as plt
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn import svm
from sklearn.neighbors import KNeighborsClassifier as KNN
from sklearn.ensemble import RandomForestClassifier as RF
from sklearn.metrics import accuracy_score
import typing


def f(X):
    return 0.26 * (X[0]**2 + X[1]**2) - 0.48 * X[0]*X[1]

def genetic_alg (varbounds, vartypes='real', func=f, curve=False, bar=False, dimensions=2, iter=10):
    algorithm_param = {
        'max_num_iteration': iter,\
        'population_size':9.,\
        'mutation_probability':0.97913332,\
        'elit_ratio': 0.11717208,\
        'crossover_probability': 0.91313551,\
        'parents_portion': 0.61999593,\
        'crossover_type':'uniform',\
        'max_iteration_without_improv':None
    }

    model = ga(function=func, 
            dimension=dimensions, 
            #variable_type=vartypes,
            variable_type_mixed=vartypes, 
            variable_boundaries=varbounds, 
            algorithm_parameters=algorithm_param,
            convergence_curve=curve,
            progress_bar=bar)
    #print(model.param)
    start = time.time()
    model.run()
    end = time.time()
    #res = np.array([list(item.values()) for item in model.output_dict['variable']])
    #print(f"\nmodel.output_dict = {res}")
    #print(f"\nmodel.output_dict = {type(model.output_dict['function'])}")
    #print(f"Время на нахождение минимума функции с помощью ЭА: {(end - start) * 10**3}мс")
    ga_time = (end - start) * 10**3
    #print(model.output_dict['function'])
    #print(ga_time)
    return model.output_dict['function'], ga_time
    
def newton ():
    start = time.time()
    root = optimize.newton(func=f, maxiter = 100, x0 = np.array([-10,10]))
    end = time.time()
    newton_time = (end - start) * 10**3
    #print(f"newton's root: {root}")
    #print(f"Время на нахождение минимума функции методом Ньютона: {(end - start) * 10**3}мс")
    #print(f(root))
    #print(newton_time)
    return f(root), newton_time
    
def count ():
    ga_results = []
    ga_times = []
    newton_results = []
    newton_times = []
    ga_varbounds = np.array([[-10,10],[-10,10]])

    for i in range(100):
        ga_res, ga_time = genetic_alg(ga_varbounds, 
                                      vartypes=np.array([['real'], ['real']]), 
                                      curve=False, 
                                      bar=True, 
                                      dimensions=2,
                                      func=f)
        ga_results.append(ga_res)
        ga_times.append(ga_time)

        newton_result, newton_time = newton()
        newton_results.append(newton_result)
        newton_times.append(newton_time)
    
    ga_results_mean = np.mean(ga_results)
    ga_results_var = np.var(ga_results)
    ga_times_mean = np.mean(ga_times)
    ga_times_var = np.var(ga_times)

    newton_results_mean = np.mean(newton_results)
    newton_results_var = np.var(newton_results)
    newton_times_mean = np.mean(newton_times)
    newton_times_var = np.var(newton_times)

    """ print(ga_results)
    print(ga_times)
    print(newton_results)
    print(newton_times) """
    #printing a table
    titles = ["          ", 
              "Genetic alg result", 
              "Genetic alg time", 
              "Newton alg result", 
              "Newton alg time"]
    data = [['Матожидание', ga_results_mean, ga_times_mean, newton_results_mean, newton_times_mean],
            ['Дисперсия', ga_results_var, ga_times_var, newton_results_var, newton_times_var]]
    format_row = "{:>22}" * (len(titles) + 1)
    print(format_row.format("", *titles))
    for team, row in zip(titles, data):
        print(format_row.format(team, *row))

def _prepare_data (path: str, return_type="ndArray"):
    X = np.genfromtxt(path, delimiter=",")
    y = np.array([])

    for i in X:
        y = np.append(y, i[5])
        
    X = np.delete(X, 5, 1)

    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=0)
    
    if return_type == "ndArray": 
        return np.array(X_train), np.array(X_test), np.array(y_train), np.array(y_test)

    if return_type == "dataframe": 
        X_train = pd.DataFrame(data=X_train)
        X_test = pd.DataFrame(data=X_test)
        y_train = pd.DataFrame(data=y_train)
        y_test = pd.DataFrame(data=y_test)
        return X_train, X_test, y_train, y_test
    
    if return_type == "list":
        #X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=0)
        return X_train.tolist(), X_test.tolist(), y_train.tolist(), y_test.tolist()

def svm_clf (X):
    X_train, X_test, y_train, y_test = _prepare_data(path="./hayes+roth/hayes-roth.data")
    kernels = ['linear', 'poly', 'rbf', 'sigmoid']
    clf = svm.SVC(kernel=kernels[X[0].astype(int)], C=X[1], decision_function_shape="ovr").fit(X_train, y_train)
    return (1 - accuracy_score(y_test, clf.predict(X_test)))

def knn_clf (X):
    X_train, X_test, y_train, y_test = _prepare_data(path="./hayes+roth/hayes-roth.data")
    metrics = ['minkowski', 'manhattan', 'cosine', 'euclidean']
    weights = ['uniform', 'distance']
    clf = KNN(n_neighbors=int(X[0]), metric=metrics[X[1].astype(int)], weights=weights[X[2].astype(int)]).fit(X_train, y_train)
    return (1 - accuracy_score(y_test, clf.predict(X_test)))

def rf_clf (X):
    X_train, X_test, y_train, y_test = _prepare_data(path="./hayes+roth/hayes-roth.data")
    clf = RF(max_depth=int(X[0]), n_estimators=int(X[1]), max_features=int(X[2])).fit(X_train, y_train)
    return (1 - accuracy_score(y_test, clf.predict(X_test)))

def classifiers ():
    #SVM
    print("\n--------- SVM parameters ---------\n")
    svm_vartypes = np.array([['int'],['real']])
    svm_varbounds = np.array([[0,3],[1,1000]])
    genetic_alg(varbounds=svm_varbounds, vartypes=svm_vartypes, func=svm_clf, curve=True, bar=True, dimensions=2)
    #kNN
    print("\n--------- SVM parameters ---------\n")
    knn_vartypes = np.array([['int'],['int'],['int']])
    knn_varbounds = np.array([[1,10],[0,3],[0,1]])
    genetic_alg(varbounds=knn_varbounds, vartypes=knn_vartypes, func=knn_clf, curve=True, bar=True, dimensions=3)
    #RF
    print("\n--------- SVM parameters ---------\n")
    rf_vartypes = np.array([['int'],['int'],['int']])
    rf_varbounds = np.array([[1,20],[1,20],[1,5]])
    genetic_alg(varbounds=rf_varbounds, vartypes=rf_vartypes, func=rf_clf, curve=True, bar=True, dimensions=3)





count()
classifiers()
print("\n--------- Genetic Algorithm Alone ---------\n")
genetic_alg(varbounds=np.array([[-10,10],[-10,10]]), 
            vartypes=np.array([['real'], ['real']]), 
            curve=True, 
            bar=True, 
            dimensions=2,
            func=f,
            iter=500)








# ------------------------------------------------------------------------------------------
""" algorithm_param = {
        'max_num_iteration': None,\
        'population_size':100,\
        'mutation_probability':0.1,\
        'elit_ratio': 0.01,\
        'crossover_probability': 0.5,\
        'parents_portion': 0.3,\
        'crossover_type':'uniform',\
        'max_iteration_without_improv':None
    } 
    
    def view_func():
    x = np.outer(np.linspace(-3, 3, 32), np.ones(32))
    y = x.copy().T # transpose
    z = 0.26*(x**2 + y**2) - 0.48 * x*y
    
    # Creating figure
    fig = plt.figure(figsize =(10, 8))
    ax = plt.axes(projection ='3d')
    
    # Creating color map
    my_cmap = plt.get_cmap('hot')

    surf = ax.plot_surface(x, y, z,
                        cmap = my_cmap,
                        edgecolor ='none')
    
    fig.colorbar(surf, ax = ax,
                shrink = 0.5, aspect = 5)
    
    ax.set_title('Surface plot')
    
    # show plot
    plt.show()
    
    """